package ci.jjk.singleselection

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var employees: ArrayList<Employee> = ArrayList()
    private var adapter: SingleAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.setLayoutManager(LinearLayoutManager(this))
        recyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        adapter = SingleAdapter(this, employees)
        recyclerView.setAdapter(adapter)

         createList()

        btnGetSelected.setOnClickListener {

            if (adapter!!.getSelected() != null) {
                showToast(adapter!!.getSelected()!!.getName()!!);
            } else {
                showToast("No Selection");
            }
        }


    }
    private fun createList() {
        employees = ArrayList()
        for (i in 0..19) {
            val employee = Employee()
            employee.setName("Employee " + (i + 1))
            employees.add(employee)
        }
        adapter!!.setEmployees(employees)
    }

    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}
