package ci.jjk.singleselection

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_employee.view.*


/**
 * Created by ADOU JOHN on 26,juillet,2020
 *  K2JTS Ltd
 *  adoujean1996@gmail.com
 */

class SingleAdapter(context: Context, employees: ArrayList<Employee>) :
    RecyclerView.Adapter<SingleAdapter.SingleViewHolder?>() {
    private val context: Context
    private var employees: ArrayList<Employee>

    // if checkedPosition = -1, there is no default selection
    // if checkedPosition = 0, 1st item is selected by default
    private var checkedPosition = 0
    fun setEmployees(employees: ArrayList<Employee>) {
        this.employees = ArrayList()
        this.employees = employees
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SingleViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.item_employee, viewGroup, false)
        return SingleViewHolder(view)
    }

    override fun onBindViewHolder(
        singleViewHolder: SingleViewHolder,
        position: Int
    ) {
        singleViewHolder.bind(employees[position])
    }

    override fun getItemCount(): Int {
        return employees.size
    }

    inner class SingleViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private val textView: TextView = itemView.textView
        private val imageView: ImageView = itemView.imageView
        fun bind(employee: Employee) {
            if (checkedPosition == -1) {
                imageView.setVisibility(View.GONE)
            } else {
                if (checkedPosition == getAdapterPosition()) {
                    imageView.setVisibility(View.VISIBLE)
                } else {
                    imageView.setVisibility(View.GONE)
                }
            }
            textView.text = employee.getName()
            itemView.setOnClickListener {
                imageView.setVisibility(View.VISIBLE)
                if (checkedPosition != getAdapterPosition()) {
                    notifyItemChanged(checkedPosition)
                    checkedPosition = getAdapterPosition()
                }
            }
        }

    }

    fun getSelected(): Employee? {
        return if (checkedPosition != -1) {
            employees[checkedPosition]
        } else null
    }

    init {
        this.context = context
        this.employees = employees
    }
}