package ci.jjk.singleselection

/**
 * Created by ADOU JOHN on 26,juillet,2020
 *  K2JTS Ltd
 *  adoujean1996@gmail.com
 */
class Employee {
    private var name: String? = null

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }
}